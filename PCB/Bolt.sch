<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="yes" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="yes" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="IDFDebug" color="4" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Rembrandt Electronics - JST XH Connectors v1-0">
<packages>
<package name="JST-XH-07-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;7&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="9.95" y1="-2.3575" x2="9.95" y2="3.3925" width="0.254" layer="21"/>
<wire x1="9.95" y1="3.3925" x2="-9.95" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-9.95" y1="3.3925" x2="-9.95" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-9.95" y1="-2.3575" x2="9.95" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-10.555" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-9.7025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-9.1675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-7.6" y1="-2.3" x2="-7.6" y2="-1.8" width="0.254" layer="21"/>
<wire x1="7.7" y1="-2.3" x2="7.7" y2="-1.8" width="0.254" layer="21"/>
<pad name="6" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" shape="long" rot="R90"/>
</package>
<package name="JST-XH-07-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;7&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="9.95" y1="-2.3575" x2="9.95" y2="3.3925" width="0.254" layer="21"/>
<wire x1="9.95" y1="3.3925" x2="-9.95" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-9.95" y1="3.3925" x2="-9.95" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-9.95" y1="-2.3575" x2="9.95" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="-2.54" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.016" rot="R90"/>
<text x="-10.555" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-9.7025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-9.1675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-7.6" y1="-2.3" x2="-7.6" y2="-1.8" width="0.254" layer="21"/>
<wire x1="7.7" y1="-2.3" x2="7.7" y2="-1.8" width="0.254" layer="21"/>
<pad name="6" x="5.08" y="0" drill="1.016" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" rot="R90"/>
</package>
<package name="JST-XH-06-PACKAGE-LONG-PAD">
<description>&lt;b&gt;JST XH Connector Long Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;6&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="8.7" y1="-2.3575" x2="8.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="8.7" y1="3.3925" x2="-8.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-8.7" y1="3.3925" x2="-8.7" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-8.7" y1="-2.3575" x2="8.7" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-9.255" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-8.3025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.9675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-6.3" y1="-2.3" x2="-6.3" y2="-1.8" width="0.254" layer="21"/>
<wire x1="6.4" y1="-2.3" x2="6.4" y2="-1.8" width="0.254" layer="21"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
</package>
<package name="JST-XH-06-PACKAGE-ROUND-PAD">
<description>&lt;b&gt;JST XH Connector Round Pads (Package)&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;6&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<wire x1="8.7" y1="-2.3575" x2="8.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="8.7" y1="3.3925" x2="-8.7" y2="3.3925" width="0.254" layer="21"/>
<wire x1="-8.7" y1="3.3925" x2="-8.7" y2="-2.3575" width="0.254" layer="21"/>
<wire x1="-8.7" y1="-2.3575" x2="8.7" y2="-2.3575" width="0.254" layer="21"/>
<pad name="3" x="-1.27" y="0" drill="1.016" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" rot="R90"/>
<pad name="1" x="-6.35" y="0" drill="1.016" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" rot="R90"/>
<text x="-9.255" y="-2.14" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-8.3025" y="3.8925" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-7.9675" y="-1.5875" size="1.016" layer="51" ratio="10">1</text>
<wire x1="-6.3" y1="-2.3" x2="-6.3" y2="-1.8" width="0.254" layer="21"/>
<wire x1="6.4" y1="-2.3" x2="6.4" y2="-1.8" width="0.254" layer="21"/>
<pad name="6" x="6.35" y="0" drill="1.016" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JST-XH-07-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;7&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="MV" x="2.54" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="2.54" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="2.54" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="2.54" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="2.54" y="-10.16" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="2.54" y="-12.7" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="2.54" y="-15.24" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-07-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-07-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JST-XH-06-PIN" prefix="X">
<description>&lt;b&gt;JST XH Connector 2 Pin&lt;/b&gt;&lt;p&gt;

Wire to board connector.

Pitch: 2,54 mm, (0.100")&lt;p&gt;
Number of pins: &lt;b&gt;6&lt;/b&gt;&lt;b&gt;&lt;P&gt;

&lt;b&gt;Created by Rembrandt Electronics&lt;/b&gt;&lt;p&gt;
&lt;b&gt;www.rembrandtelectronics.com&lt;/b&gt;&lt;p&gt;</description>
<gates>
<gate name="-1" symbol="MV" x="2.54" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="M" x="2.54" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="2.54" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="2.54" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="2.54" y="-10.16" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="2.54" y="-12.7" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="-LONG-PAD" package="JST-XH-06-PACKAGE-LONG-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-ROUND-PAD" package="JST-XH-06-PACKAGE-ROUND-PAD">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Pololu_S7V8F3">
<packages>
<package name="S7V8F3">
<pad name="VOUT" x="-3.81" y="0" drill="1.1" shape="octagon"/>
<pad name="GND" x="-1.27" y="0" drill="1.1" shape="octagon"/>
<pad name="VIN" x="1.27" y="0" drill="1.1" shape="octagon"/>
<pad name="!SHDN" x="3.81" y="0" drill="1.1" shape="octagon"/>
<wire x1="-5.75" y1="-1.27" x2="5.75" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.75" y1="-1.27" x2="5.75" y2="15" width="0.127" layer="21"/>
<wire x1="5.75" y1="15" x2="-5.75" y2="15" width="0.127" layer="21"/>
<wire x1="-5.75" y1="15" x2="-5.75" y2="-1.27" width="0.127" layer="21"/>
<text x="-3.81" y="6.35" size="1.27" layer="25">Pololu
S7V8F3</text>
<text x="-3.175" y="1.27" size="0.8128" layer="21" rot="R90">VOUT</text>
<text x="-0.635" y="1.27" size="0.8128" layer="21" rot="R90">GND</text>
<text x="1.905" y="1.27" size="0.8128" layer="21" rot="R90">VIN</text>
<text x="4.445" y="1.27" size="0.8128" layer="21" rot="R90">!SHDN</text>
</package>
</packages>
<symbols>
<symbol name="S7V8F3">
<pin name="VIN" x="-10.16" y="5.08" visible="off" length="middle"/>
<pin name="VOUT" x="10.16" y="5.08" visible="off" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-10.16" visible="off" length="middle" rot="R90"/>
<pin name="!SHDN" x="10.16" y="-2.54" visible="off" length="middle" rot="R180"/>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="97"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="7.62" width="0.254" layer="97"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="97"/>
<text x="-1.905" y="5.715" size="1.016" layer="97" rot="R180">VIN</text>
<text x="4.445" y="5.715" size="1.016" layer="97" rot="R180">VOUT</text>
<text x="0.635" y="-3.175" size="1.016" layer="97">!SHDN</text>
<text x="-3.175" y="-6.985" size="1.016" layer="97">GND</text>
<text x="-5.715" y="8.255" size="1.016" layer="97">Pololu S7V8F3</text>
<text x="5.715" y="3.81" size="0.8128" layer="97">3.3V</text>
<text x="-8.89" y="1.27" size="0.8128" layer="97">2.7V
-
11.8V</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="S7V8F3">
<description>Pololu S7V8F3 - Pololu 3.3V Step-Up/Step-Down Voltage Regulator S7V8F3 - The S7V8F3 switching step-up/step-down regulator efficiently produces a fixed 3.3 V output from input voltages between 2.7 V and 11.8 V. Its ability to convert both higher and lower input voltages makes it useful for applications where the power supply voltage can vary greatly, as with batteries that start above but discharge below the regulated voltage. The compact module has a typical efficiency of over 90% and can deliver 500 mA to 1 A across most of the input voltage range.</description>
<gates>
<gate name="G$1" symbol="S7V8F3" x="5.08" y="5.08"/>
</gates>
<devices>
<device name="" package="S7V8F3">
<connects>
<connect gate="G$1" pin="!SHDN" pad="!SHDN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
<connect gate="G$1" pin="VOUT" pad="VOUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MPU-9250">
<packages>
<package name="QFN40P300X300X105-24N">
<wire x1="-1.55" y1="1.55" x2="1.55" y2="1.55" width="0.127" layer="51"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="-1.55" width="0.127" layer="51"/>
<wire x1="1.55" y1="-1.55" x2="-1.55" y2="-1.55" width="0.127" layer="51"/>
<wire x1="-1.55" y1="-1.55" x2="-1.55" y2="1.55" width="0.127" layer="51"/>
<wire x1="-1.3" y1="1.55" x2="-1.55" y2="1.55" width="0.127" layer="21"/>
<wire x1="-1.55" y1="1.55" x2="-1.55" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.3" y1="1.55" x2="1.55" y2="1.55" width="0.127" layer="21"/>
<wire x1="1.55" y1="1.55" x2="1.55" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.55" y1="-1.3" x2="1.55" y2="-1.55" width="0.127" layer="21"/>
<wire x1="1.55" y1="-1.55" x2="1.3" y2="-1.55" width="0.127" layer="21"/>
<wire x1="-1.55" y1="-1.3" x2="-1.55" y2="-1.55" width="0.127" layer="21"/>
<wire x1="-1.55" y1="-1.55" x2="-1.3" y2="-1.55" width="0.127" layer="21"/>
<wire x1="-2.12" y1="2.12" x2="2.12" y2="2.12" width="0.05" layer="39"/>
<wire x1="2.12" y1="2.12" x2="2.12" y2="-2.12" width="0.05" layer="39"/>
<wire x1="2.12" y1="-2.12" x2="-2.12" y2="-2.12" width="0.05" layer="39"/>
<wire x1="-2.12" y1="-2.12" x2="-2.12" y2="2.12" width="0.05" layer="39"/>
<circle x="-2.4" y="1" radius="0.1" width="0.2" layer="21"/>
<circle x="-1.2" y="1.1" radius="0.1" width="0.2" layer="51"/>
<text x="-2.544409375" y="2.544409375" size="1.272209375" layer="25">&gt;NAME</text>
<text x="-2.541240625" y="-3.811859375" size="1.27061875" layer="27">&gt;VALUE</text>
<rectangle x1="-0.85" y1="-0.77" x2="0.85" y2="0.77" layer="41"/>
<rectangle x1="-0.85" y1="-0.77" x2="0.85" y2="0.77" layer="43"/>
<smd name="1" x="-1.545" y="1" dx="0.64" dy="0.22" layer="1" roundness="25"/>
<smd name="2" x="-1.545" y="0.6" dx="0.64" dy="0.22" layer="1" roundness="25"/>
<smd name="3" x="-1.545" y="0.2" dx="0.64" dy="0.22" layer="1" roundness="25"/>
<smd name="4" x="-1.545" y="-0.2" dx="0.64" dy="0.22" layer="1" roundness="25"/>
<smd name="5" x="-1.545" y="-0.6" dx="0.64" dy="0.22" layer="1" roundness="25"/>
<smd name="6" x="-1.545" y="-1" dx="0.64" dy="0.22" layer="1" roundness="25"/>
<smd name="7" x="-1" y="-1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R90"/>
<smd name="8" x="-0.6" y="-1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R90"/>
<smd name="9" x="-0.2" y="-1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="0.2" y="-1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="0.6" y="-1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="1" y="-1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R90"/>
<smd name="13" x="1.545" y="-1" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R180"/>
<smd name="14" x="1.545" y="-0.6" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R180"/>
<smd name="15" x="1.545" y="-0.2" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R180"/>
<smd name="16" x="1.545" y="0.2" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R180"/>
<smd name="17" x="1.545" y="0.6" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R180"/>
<smd name="18" x="1.545" y="1" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R180"/>
<smd name="19" x="1" y="1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R270"/>
<smd name="20" x="0.6" y="1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R270"/>
<smd name="21" x="0.2" y="1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R270"/>
<smd name="22" x="-0.2" y="1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R270"/>
<smd name="23" x="-0.6" y="1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R270"/>
<smd name="24" x="-1" y="1.545" dx="0.64" dy="0.22" layer="1" roundness="25" rot="R270"/>
</package>
</packages>
<symbols>
<symbol name="MPU-9250">
<wire x1="15.24" y1="20.32" x2="-15.24" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="20.32" x2="-15.24" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-17.78" x2="15.24" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-17.78" x2="15.24" y2="20.32" width="0.1524" layer="94"/>
<text x="-15.2602" y="20.8556" size="1.780359375" layer="95">&gt;NAME</text>
<text x="-15.2414" y="-20.3219" size="1.778159375" layer="96">&gt;VALUE</text>
<pin name="FSYNC" x="-20.32" y="10.16" length="middle" direction="in"/>
<pin name="VDDIO" x="20.32" y="15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="INT" x="20.32" y="10.16" length="middle" direction="out" rot="R180"/>
<pin name="AD0/SDO" x="-20.32" y="-2.54" length="middle" direction="out"/>
<pin name="REGOUT" x="20.32" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="VDD" x="20.32" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="20.32" y="-15.24" length="middle" direction="pwr" rot="R180"/>
<pin name="RESV_19" x="20.32" y="-2.54" length="middle" direction="pas" rot="R180"/>
<pin name="RESV_20" x="20.32" y="-5.08" length="middle" direction="pas" rot="R180"/>
<pin name="AUX_DA" x="-20.32" y="-7.62" length="middle" direction="in"/>
<pin name="SCL/SCLK" x="-20.32" y="2.54" length="middle" direction="in" function="clk"/>
<pin name="!CS" x="-20.32" y="5.08" length="middle" direction="in"/>
<pin name="SDA/SDI" x="-20.32" y="0" length="middle"/>
<pin name="AUX_CL" x="-20.32" y="-5.08" length="middle" direction="in" function="clk"/>
<pin name="RESV_1" x="20.32" y="0" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MPU-9250" prefix="U">
<description>Axis=3 Axis Gyro+3 Axis Accel+Compass. Pin For Pin With m &lt;a href="https://pricing.snapeda.com/parts/MPU-9250/TDK%20InvenSense/view-part?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MPU-9250" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN40P300X300X105-24N">
<connects>
<connect gate="G$1" pin="!CS" pad="22"/>
<connect gate="G$1" pin="AD0/SDO" pad="9"/>
<connect gate="G$1" pin="AUX_CL" pad="7"/>
<connect gate="G$1" pin="AUX_DA" pad="21"/>
<connect gate="G$1" pin="FSYNC" pad="11"/>
<connect gate="G$1" pin="GND" pad="18"/>
<connect gate="G$1" pin="INT" pad="12"/>
<connect gate="G$1" pin="REGOUT" pad="10"/>
<connect gate="G$1" pin="RESV_1" pad="1"/>
<connect gate="G$1" pin="RESV_19" pad="19"/>
<connect gate="G$1" pin="RESV_20" pad="20"/>
<connect gate="G$1" pin="SCL/SCLK" pad="23"/>
<connect gate="G$1" pin="SDA/SDI" pad="24"/>
<connect gate="G$1" pin="VDD" pad="13"/>
<connect gate="G$1" pin="VDDIO" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Bad"/>
<attribute name="DESCRIPTION" value=" Accelerometer, Gyroscope, Magnetometer, 9 Axis Sensor I²C, SPI Output "/>
<attribute name="MF" value="TDK InvenSense"/>
<attribute name="MP" value="MPU-9250"/>
<attribute name="PACKAGE" value="TFQFN-24 TDK InvenSense"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ESP32-WROOM-32D">
<packages>
<package name="MODULE_ESP32-WROOM-32D">
<wire x1="-9" y1="12.75" x2="9" y2="12.75" width="0.127" layer="51"/>
<wire x1="9" y1="12.75" x2="9" y2="6.45" width="0.127" layer="51"/>
<wire x1="9" y1="6.45" x2="9" y2="-12.75" width="0.127" layer="51"/>
<wire x1="9" y1="-12.75" x2="-9" y2="-12.75" width="0.127" layer="51"/>
<wire x1="-9" y1="-12.75" x2="-9" y2="6.45" width="0.127" layer="51"/>
<wire x1="-9" y1="12.75" x2="-9" y2="6.45" width="0.127" layer="51"/>
<wire x1="-9" y1="6.45" x2="9" y2="6.45" width="0.127" layer="51"/>
<rectangle x1="-9" y1="6.45" x2="9" y2="12.75" layer="41"/>
<rectangle x1="-9" y1="6.45" x2="9" y2="12.75" layer="43"/>
<text x="-6" y="9" size="1.778" layer="51">ANTENNA</text>
<wire x1="-9.25" y1="13" x2="9.25" y2="13" width="0.05" layer="39"/>
<wire x1="9.25" y1="13" x2="9.25" y2="6" width="0.05" layer="39"/>
<wire x1="9.25" y1="6" x2="9.75" y2="6" width="0.05" layer="39"/>
<wire x1="9.75" y1="6" x2="9.75" y2="-13.5" width="0.05" layer="39"/>
<wire x1="9.75" y1="-13.5" x2="-9.75" y2="-13.5" width="0.05" layer="39"/>
<wire x1="-9.75" y1="-13.5" x2="-9.75" y2="6" width="0.05" layer="39"/>
<wire x1="-9.75" y1="6" x2="-9.25" y2="6" width="0.05" layer="39"/>
<wire x1="-9.25" y1="6" x2="-9.25" y2="13" width="0.05" layer="39"/>
<circle x="-10" y="5.25" radius="0.1" width="0.2" layer="51"/>
<circle x="-10" y="5.25" radius="0.1" width="0.2" layer="21"/>
<text x="-9.5" y="13.25" size="1.27" layer="25">&gt;NAME</text>
<text x="-9.5" y="-15" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9" y1="-12.1" x2="-9" y2="-12.75" width="0.127" layer="21"/>
<wire x1="-9" y1="-12.75" x2="-6.55" y2="-12.75" width="0.127" layer="21"/>
<wire x1="6.55" y1="-12.75" x2="9" y2="-12.75" width="0.127" layer="21"/>
<wire x1="9" y1="-12.75" x2="9" y2="-12.1" width="0.127" layer="21"/>
<wire x1="-9" y1="6.25" x2="-9" y2="12.75" width="0.127" layer="21"/>
<wire x1="-9" y1="12.75" x2="9" y2="12.75" width="0.127" layer="21"/>
<wire x1="9" y1="12.75" x2="9" y2="6.25" width="0.127" layer="21"/>
<smd name="1" x="-8.5" y="5.26" dx="2" dy="0.9" layer="1"/>
<smd name="2" x="-8.5" y="3.99" dx="2" dy="0.9" layer="1"/>
<smd name="3" x="-8.5" y="2.72" dx="2" dy="0.9" layer="1"/>
<smd name="4" x="-8.5" y="1.45" dx="2" dy="0.9" layer="1"/>
<smd name="5" x="-8.5" y="0.18" dx="2" dy="0.9" layer="1"/>
<smd name="6" x="-8.5" y="-1.09" dx="2" dy="0.9" layer="1"/>
<smd name="7" x="-8.5" y="-2.36" dx="2" dy="0.9" layer="1"/>
<smd name="8" x="-8.5" y="-3.63" dx="2" dy="0.9" layer="1"/>
<smd name="9" x="-8.5" y="-4.9" dx="2" dy="0.9" layer="1"/>
<smd name="10" x="-8.5" y="-6.17" dx="2" dy="0.9" layer="1"/>
<smd name="11" x="-8.5" y="-7.44" dx="2" dy="0.9" layer="1"/>
<smd name="12" x="-8.5" y="-8.71" dx="2" dy="0.9" layer="1"/>
<smd name="13" x="-8.5" y="-9.98" dx="2" dy="0.9" layer="1"/>
<smd name="14" x="-8.5" y="-11.25" dx="2" dy="0.9" layer="1"/>
<smd name="15" x="-5.715" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="16" x="-4.445" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="17" x="-3.175" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="18" x="-1.905" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="19" x="-0.635" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="20" x="0.635" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="21" x="1.905" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="22" x="3.175" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="23" x="4.445" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="24" x="5.715" y="-12.25" dx="0.9" dy="2" layer="1"/>
<smd name="25" x="8.5" y="-11.25" dx="2" dy="0.9" layer="1"/>
<smd name="26" x="8.5" y="-9.98" dx="2" dy="0.9" layer="1"/>
<smd name="27" x="8.5" y="-8.71" dx="2" dy="0.9" layer="1"/>
<smd name="28" x="8.5" y="-7.44" dx="2" dy="0.9" layer="1"/>
<smd name="29" x="8.5" y="-6.17" dx="2" dy="0.9" layer="1"/>
<smd name="30" x="8.5" y="-4.9" dx="2" dy="0.9" layer="1"/>
<smd name="31" x="8.5" y="-3.63" dx="2" dy="0.9" layer="1"/>
<smd name="32" x="8.5" y="-2.36" dx="2" dy="0.9" layer="1"/>
<smd name="33" x="8.5" y="-1.09" dx="2" dy="0.9" layer="1"/>
<smd name="34" x="8.5" y="0.18" dx="2" dy="0.9" layer="1"/>
<smd name="35" x="8.5" y="1.45" dx="2" dy="0.9" layer="1"/>
<smd name="36" x="8.5" y="2.72" dx="2" dy="0.9" layer="1"/>
<smd name="37" x="8.5" y="3.99" dx="2" dy="0.9" layer="1"/>
<smd name="38" x="8.5" y="5.26" dx="2" dy="0.9" layer="1"/>
<smd name="39_1" x="-2.835" y="-0.405" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_2" x="-1" y="-0.405" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_3" x="0.835" y="-0.405" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_4" x="-2.835" y="-2.24" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_5" x="-1" y="-2.24" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_6" x="0.835" y="-2.24" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_7" x="-2.835" y="-4.075" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_8" x="-1" y="-4.075" dx="1.33" dy="1.33" layer="1"/>
<smd name="39_9" x="0.835" y="-4.075" dx="1.33" dy="1.33" layer="1"/>
<pad name="39_10" x="-1.9175" y="-0.405" drill="0.2" diameter="0.3"/>
<pad name="39_11" x="-0.0825" y="-0.405" drill="0.2" diameter="0.3"/>
<pad name="39_12" x="-2.835" y="-1.3225" drill="0.2" diameter="0.3"/>
<pad name="39_13" x="-1" y="-1.3225" drill="0.2" diameter="0.3"/>
<pad name="39_14" x="0.835" y="-1.3225" drill="0.2" diameter="0.3"/>
<pad name="39_15" x="-1.9175" y="-2.24" drill="0.2" diameter="0.3"/>
<pad name="39_16" x="-0.0825" y="-2.24" drill="0.2" diameter="0.3"/>
<pad name="39_17" x="-2.835" y="-3.1575" drill="0.2" diameter="0.3"/>
<pad name="39_18" x="-1" y="-3.1575" drill="0.2" diameter="0.3"/>
<pad name="39_19" x="0.835" y="-3.1575" drill="0.2" diameter="0.3"/>
<pad name="39_20" x="-1.9175" y="-4.075" drill="0.2" diameter="0.3"/>
<pad name="39_21" x="-0.0825" y="-4.075" drill="0.2" diameter="0.3"/>
</package>
</packages>
<symbols>
<symbol name="ESP32-WROOM-32D">
<wire x1="-15.24" y1="30.48" x2="15.24" y2="30.48" width="0.254" layer="94"/>
<wire x1="15.24" y1="30.48" x2="15.24" y2="-33.02" width="0.254" layer="94"/>
<wire x1="15.24" y1="-33.02" x2="-15.24" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-33.02" x2="-15.24" y2="30.48" width="0.254" layer="94"/>
<text x="-15.0359" y="31.5945" size="1.778" layer="95">&gt;NAME</text>
<text x="-15.2867" y="-35.6681" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="20.32" y="-30.48" length="middle" direction="pwr" rot="R180"/>
<pin name="3V3" x="20.32" y="27.94" length="middle" direction="pwr" rot="R180"/>
<pin name="EN" x="-20.32" y="22.86" length="middle" direction="in"/>
<pin name="SENSOR_VP" x="-20.32" y="15.24" length="middle" direction="in"/>
<pin name="SENSOR_VN" x="-20.32" y="12.7" length="middle" direction="in"/>
<pin name="IO34" x="-20.32" y="7.62" length="middle" direction="in"/>
<pin name="IO35" x="-20.32" y="5.08" length="middle" direction="in"/>
<pin name="IO33" x="20.32" y="-25.4" length="middle" rot="R180"/>
<pin name="IO32" x="20.32" y="-22.86" length="middle" rot="R180"/>
<pin name="IO25" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="IO26" x="20.32" y="-17.78" length="middle" rot="R180"/>
<pin name="IO27" x="20.32" y="-20.32" length="middle" rot="R180"/>
<pin name="IO14" x="20.32" y="7.62" length="middle" rot="R180"/>
<pin name="IO12" x="20.32" y="12.7" length="middle" rot="R180"/>
<pin name="IO13" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="IO15" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="IO2" x="20.32" y="20.32" length="middle" rot="R180"/>
<pin name="IO0" x="20.32" y="22.86" length="middle" rot="R180"/>
<pin name="IO4" x="20.32" y="17.78" length="middle" rot="R180"/>
<pin name="IO16" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="IO17" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="IO5" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="IO18" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="IO19" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="IO21" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="IO22" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="IO23" x="20.32" y="-12.7" length="middle" rot="R180"/>
<pin name="RXD0" x="-20.32" y="-2.54" length="middle"/>
<pin name="TXD0" x="-20.32" y="0" length="middle"/>
<pin name="SHD/SD2" x="-20.32" y="-22.86" length="middle"/>
<pin name="SWP/SD3" x="-20.32" y="-25.4" length="middle"/>
<pin name="SCS/CMD" x="-20.32" y="-7.62" length="middle"/>
<pin name="SCK/CLK" x="-20.32" y="-10.16" length="middle" function="clk"/>
<pin name="SDO/SD0" x="-20.32" y="-15.24" length="middle"/>
<pin name="SDI/SD1" x="-20.32" y="-17.78" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ESP32-WROOM-32D" prefix="U">
<description>Bluetooth, WiFi 802.11b/g/n, Bluetooth v4.2 + EDR, Class 1, 2 and 3 Transceiver Module 2.4GHz ~ 2.5GHz Integrated, Trace Surface Mount &lt;a href="https://pricing.snapeda.com/parts/ESP32-WROOM-32D/Espressif%20Systems/view-part?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ESP32-WROOM-32D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MODULE_ESP32-WROOM-32D">
<connects>
<connect gate="G$1" pin="3V3" pad="2"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="1 15 38 39_1 39_2 39_3 39_4 39_5 39_6 39_7 39_8 39_9 39_10 39_11 39_12 39_13 39_14 39_15 39_16 39_17 39_18 39_19 39_20 39_21"/>
<connect gate="G$1" pin="IO0" pad="25"/>
<connect gate="G$1" pin="IO12" pad="14"/>
<connect gate="G$1" pin="IO13" pad="16"/>
<connect gate="G$1" pin="IO14" pad="13"/>
<connect gate="G$1" pin="IO15" pad="23"/>
<connect gate="G$1" pin="IO16" pad="27"/>
<connect gate="G$1" pin="IO17" pad="28"/>
<connect gate="G$1" pin="IO18" pad="30"/>
<connect gate="G$1" pin="IO19" pad="31"/>
<connect gate="G$1" pin="IO2" pad="24"/>
<connect gate="G$1" pin="IO21" pad="33"/>
<connect gate="G$1" pin="IO22" pad="36"/>
<connect gate="G$1" pin="IO23" pad="37"/>
<connect gate="G$1" pin="IO25" pad="10"/>
<connect gate="G$1" pin="IO26" pad="11"/>
<connect gate="G$1" pin="IO27" pad="12"/>
<connect gate="G$1" pin="IO32" pad="8"/>
<connect gate="G$1" pin="IO33" pad="9"/>
<connect gate="G$1" pin="IO34" pad="6"/>
<connect gate="G$1" pin="IO35" pad="7"/>
<connect gate="G$1" pin="IO4" pad="26"/>
<connect gate="G$1" pin="IO5" pad="29"/>
<connect gate="G$1" pin="RXD0" pad="34"/>
<connect gate="G$1" pin="SCK/CLK" pad="20"/>
<connect gate="G$1" pin="SCS/CMD" pad="19"/>
<connect gate="G$1" pin="SDI/SD1" pad="22"/>
<connect gate="G$1" pin="SDO/SD0" pad="21"/>
<connect gate="G$1" pin="SENSOR_VN" pad="5"/>
<connect gate="G$1" pin="SENSOR_VP" pad="4"/>
<connect gate="G$1" pin="SHD/SD2" pad="17"/>
<connect gate="G$1" pin="SWP/SD3" pad="18"/>
<connect gate="G$1" pin="TXD0" pad="35"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Bad"/>
<attribute name="DESCRIPTION" value=" Bluetooth, WiFi 802.11b/g/n, Bluetooth v4.2 +EDR, Class 1, 2 and 3 Transceiver Module 2.4GHz ~ 2.5GHz Integrated, Trace Surface Mount "/>
<attribute name="MF" value="Espressif Systems"/>
<attribute name="MP" value="ESP32-WROOM-32D"/>
<attribute name="PACKAGE" value="Module Espressif Systems"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="D36V28FX">
<description>&lt;5V, 3.2A Step-Down Voltage Regulator D36V28F5&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="D36V28FX">
<description>&lt;b&gt;D36V28FX-2&lt;/b&gt;&lt;br&gt;
</description>
<pad name="1" x="0" y="0" drill="1.02" diameter="1.53"/>
<pad name="2" x="0" y="-2.54" drill="1.02" diameter="1.53"/>
<pad name="3" x="2.54" y="-2.54" drill="1.02" diameter="1.53"/>
<pad name="4" x="5.08" y="-2.54" drill="1.02" diameter="1.53"/>
<pad name="5" x="7.62" y="-2.54" drill="1.02" diameter="1.53"/>
<pad name="6" x="10.16" y="-2.54" drill="1.02" diameter="1.53"/>
<hole x="-4.17" y="-1.61" drill="2.18"/>
<hole x="9.33" y="14.39" drill="2.18"/>
<text x="2.53" y="5.93" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="2.53" y="5.93" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.37" y1="16.075" x2="11.43" y2="16.075" width="0.2" layer="51"/>
<wire x1="11.43" y1="16.075" x2="11.43" y2="-4.225" width="0.2" layer="51"/>
<wire x1="11.43" y1="-4.225" x2="-6.37" y2="-4.225" width="0.2" layer="51"/>
<wire x1="-6.37" y1="-4.225" x2="-6.37" y2="16.075" width="0.2" layer="51"/>
<wire x1="-6.37" y1="16.085" x2="11.43" y2="16.085" width="0.1" layer="21"/>
<wire x1="11.43" y1="16.085" x2="11.43" y2="-4.225" width="0.1" layer="21"/>
<wire x1="11.43" y1="-4.225" x2="-6.37" y2="-4.225" width="0.1" layer="21"/>
<wire x1="-6.37" y1="-4.225" x2="-6.37" y2="16.085" width="0.1" layer="21"/>
<wire x1="-7.37" y1="17.085" x2="12.43" y2="17.085" width="0.1" layer="51"/>
<wire x1="12.43" y1="17.085" x2="12.43" y2="-5.225" width="0.1" layer="51"/>
<wire x1="12.43" y1="-5.225" x2="-7.37" y2="-5.225" width="0.1" layer="51"/>
<wire x1="-7.37" y1="-5.225" x2="-7.37" y2="17.085" width="0.1" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="D36V28FX">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-12.7" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="PG" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="EN" x="0" y="0" length="middle"/>
<pin name="IN" x="0" y="-2.54" length="middle"/>
<pin name="GND_1" x="0" y="-5.08" length="middle"/>
<pin name="GND_2" x="0" y="-7.62" length="middle"/>
<pin name="OUT" x="0" y="-10.16" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="D36V28FX" prefix="IC">
<description>&lt;b&gt;5V, 3.2A Step-Down Voltage Regulator D36V28F5&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://www.pololu.com/product/3782"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="D36V28FX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="D36V28FX">
<connects>
<connect gate="G$1" pin="EN" pad="2"/>
<connect gate="G$1" pin="GND_1" pad="4"/>
<connect gate="G$1" pin="GND_2" pad="5"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="6"/>
<connect gate="G$1" pin="PG" pad="1"/>
</connects>
<technologies>
<technology name="">
<attribute name="ARROW_PART_NUMBER" value="" constant="no"/>
<attribute name="ARROW_PRICE-STOCK" value="" constant="no"/>
<attribute name="DESCRIPTION" value="5V, 3.2A Step-Down Voltage Regulator D36V28F5" constant="no"/>
<attribute name="HEIGHT" value="8.77mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="POLOLU ROBOTICS &amp; ELECTRONICS" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="D36V28FX" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
<attribute name="MOUSER_PRICE-STOCK" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TP4056">
<packages>
<package name="SOT23-8">
<description>&lt;b&gt;Package SOP-8PP &lt;/b&gt; http://goo.gl/2DAeEi</description>
<circle x="-2.05" y="-1.6" radius="0.1" width="0" layer="21"/>
<smd name="1" x="-1.905" y="-2.58" dx="0.42" dy="0.835" layer="1"/>
<smd name="2" x="-0.635" y="-2.58" dx="0.42" dy="0.835" layer="1"/>
<smd name="3" x="0.635" y="-2.58" dx="0.42" dy="0.835" layer="1"/>
<smd name="6" x="0.635" y="2.58" dx="0.42" dy="0.835" layer="1"/>
<smd name="7" x="-0.635" y="2.58" dx="0.42" dy="0.835" layer="1"/>
<smd name="8" x="-1.905" y="2.58" dx="0.42" dy="0.835" layer="1"/>
<text x="-1.397" y="-3.942" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<text x="-1.397" y="2.972" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<smd name="5" x="1.905" y="2.58" dx="0.42" dy="0.835" layer="1"/>
<smd name="4" x="1.905" y="-2.58" dx="0.42" dy="0.835" layer="1"/>
<wire x1="2.45" y1="-1.95" x2="2.45" y2="1.95" width="0.127" layer="21"/>
<wire x1="-2.45" y1="-1.95" x2="-2.45" y2="1.95" width="0.127" layer="21"/>
<wire x1="-2.45" y1="1.95" x2="2.45" y2="1.95" width="0.127" layer="21"/>
<wire x1="-2.45" y1="-1.95" x2="2.45" y2="-1.95" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TP4056">
<description>Data sheet: http://goo.gl/J7k8TF</description>
<wire x1="-7.62" y1="10.16" x2="12.7" y2="10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="10.16" x2="12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="12.7" y1="-10.16" x2="-7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-7.62" y="-12.7" size="1.778" layer="95">&gt;NAME</text>
<pin name="TEMP" x="-10.16" y="7.62" length="short"/>
<pin name="PROG" x="-10.16" y="2.54" length="short"/>
<pin name="GND" x="-10.16" y="-2.54" length="short"/>
<pin name="VCC" x="-10.16" y="-7.62" length="short"/>
<pin name="BAT" x="15.24" y="-7.62" length="short" rot="R180"/>
<pin name="!STDBY" x="15.24" y="-2.54" length="short" rot="R180"/>
<pin name="!CHRG" x="15.24" y="2.54" length="short" rot="R180"/>
<pin name="CE" x="15.24" y="7.62" length="short" rot="R180"/>
<text x="-7.62" y="-15.24" size="1.27" layer="94">TP4056</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TP4056">
<description>&lt;b&gt;TP4056&lt;/b&gt;

&lt;p&gt;&lt;a href="http://goo.gl/J7k8TF"&gt;Data sheet&lt;/a&gt;&lt;/p&gt;

Dave.Calaway &lt;a href="https://goo.gl/Muy1Tr"&gt;Eagle Library&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TP4056" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-8">
<connects>
<connect gate="G$1" pin="!CHRG" pad="7"/>
<connect gate="G$1" pin="!STDBY" pad="6"/>
<connect gate="G$1" pin="BAT" pad="5"/>
<connect gate="G$1" pin="CE" pad="8"/>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="PROG" pad="2"/>
<connect gate="G$1" pin="TEMP" pad="1"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="DIST_A" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-07-PIN" device="-LONG-PAD"/>
<part name="DIST_B" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-07-PIN" device="-LONG-PAD"/>
<part name="DIST_C" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-07-PIN" device="-LONG-PAD"/>
<part name="HALL_A" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-06-PIN" device="-LONG-PAD"/>
<part name="HALL_B" library="Rembrandt Electronics - JST XH Connectors v1-0" deviceset="JST-XH-06-PIN" device="-LONG-PAD"/>
<part name="U$1" library="Pololu_S7V8F3" deviceset="S7V8F3" device=""/>
<part name="U1" library="MPU-9250" deviceset="MPU-9250" device=""/>
<part name="U2" library="ESP32-WROOM-32D" deviceset="ESP32-WROOM-32D" device=""/>
<part name="IC1" library="D36V28FX" deviceset="D36V28FX" device=""/>
<part name="U$2" library="TP4056" deviceset="TP4056" device=""/>
</parts>
<sheets>
<sheet>
<description>Power supply</description>
<plain>
</plain>
<instances>
<instance part="U$1" gate="G$1" x="2.54" y="76.2" smashed="yes"/>
<instance part="IC1" gate="G$1" x="30.48" y="81.28" smashed="yes">
<attribute name="NAME" x="54.61" y="88.9" size="1.778" layer="95" align="center-left"/>
<attribute name="VALUE" x="54.61" y="86.36" size="1.778" layer="96" align="center-left"/>
</instance>
<instance part="U$2" gate="G$1" x="38.1" y="45.72" smashed="yes">
<attribute name="NAME" x="30.48" y="33.02" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<description>MCU</description>
<plain>
</plain>
<instances>
<instance part="U2" gate="G$1" x="60.96" y="48.26" smashed="yes">
<attribute name="NAME" x="45.9241" y="79.8545" size="1.778" layer="95"/>
<attribute name="VALUE" x="45.6733" y="12.5919" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$28" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="3V3"/>
<wire x1="81.28" y1="76.2" x2="93.98" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IO0"/>
<wire x1="81.28" y1="71.12" x2="93.98" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IO2"/>
<wire x1="81.28" y1="68.58" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IO4"/>
<wire x1="81.28" y1="66.04" x2="93.98" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IO5"/>
<wire x1="81.28" y1="63.5" x2="93.98" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="81.28" y1="60.96" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO12"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="81.28" y1="58.42" x2="93.98" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO13"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="81.28" y1="55.88" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO14"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="81.28" y1="53.34" x2="93.98" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO15"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="81.28" y1="50.8" x2="93.98" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO16"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="81.28" y1="48.26" x2="93.98" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO17"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="81.28" y1="45.72" x2="93.98" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO18"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="81.28" y1="43.18" x2="93.98" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO19"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="81.28" y1="40.64" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO21"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="81.28" y1="38.1" x2="93.98" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO22"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="81.28" y1="35.56" x2="93.98" y2="35.56" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO23"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<wire x1="81.28" y1="33.02" x2="93.98" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO25"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<wire x1="81.28" y1="30.48" x2="93.98" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO26"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="81.28" y1="27.94" x2="93.98" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO27"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<wire x1="81.28" y1="25.4" x2="93.98" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO32"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<wire x1="81.28" y1="22.86" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO33"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<wire x1="81.28" y1="17.78" x2="93.98" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="EN"/>
<wire x1="40.64" y1="71.12" x2="27.94" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SENSOR_VP"/>
<wire x1="40.64" y1="63.5" x2="27.94" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="SENSOR_VN"/>
<wire x1="40.64" y1="60.96" x2="27.94" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<wire x1="40.64" y1="55.88" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO34"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<wire x1="40.64" y1="53.34" x2="27.94" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="IO35"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<wire x1="40.64" y1="48.26" x2="27.94" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="TXD0"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<wire x1="40.64" y1="45.72" x2="27.94" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="RXD0"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<wire x1="40.64" y1="40.64" x2="27.94" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SCS/CMD"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<wire x1="40.64" y1="38.1" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SCK/CLK"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="40.64" y1="33.02" x2="27.94" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SDO/SD0"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<wire x1="40.64" y1="30.48" x2="27.94" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SDI/SD1"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="40.64" y1="25.4" x2="27.94" y2="25.4" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SHD/SD2"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<wire x1="40.64" y1="22.86" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SWP/SD3"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Motors driver</description>
<plain>
</plain>
<instances>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<description>Acceleration &amp; position sensor</description>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="68.58" y="45.72" smashed="yes">
<attribute name="NAME" x="53.3198" y="66.5756" size="1.780359375" layer="95"/>
<attribute name="VALUE" x="53.3386" y="25.3981" size="1.778159375" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$69" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="FSYNC"/>
<wire x1="48.26" y1="55.88" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="!CS"/>
<wire x1="48.26" y1="50.8" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SCL/SCLK"/>
<wire x1="48.26" y1="48.26" x2="35.56" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="SDA/SDI"/>
<wire x1="48.26" y1="45.72" x2="35.56" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="48.26" y1="43.18" x2="35.56" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="AD0/SDO"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<wire x1="48.26" y1="40.64" x2="35.56" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="AUX_CL"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<wire x1="48.26" y1="38.1" x2="35.56" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="AUX_DA"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<wire x1="101.6" y1="45.72" x2="88.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RESV_1"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="101.6" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RESV_19"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<wire x1="101.6" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="RESV_20"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDD"/>
<wire x1="88.9" y1="63.5" x2="101.6" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDIO"/>
<wire x1="88.9" y1="60.96" x2="101.6" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="INT"/>
<wire x1="88.9" y1="55.88" x2="101.6" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="REGOUT"/>
<wire x1="88.9" y1="50.8" x2="101.6" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="88.9" y1="30.48" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>External pins</description>
<plain>
</plain>
<instances>
<instance part="DIST_A" gate="-1" x="-2.54" y="76.2" smashed="yes">
<attribute name="NAME" x="0" y="75.438" size="1.524" layer="95"/>
<attribute name="VALUE" x="-3.302" y="77.597" size="1.778" layer="96"/>
</instance>
<instance part="DIST_A" gate="-2" x="-2.54" y="73.66" smashed="yes">
<attribute name="NAME" x="0" y="72.898" size="1.524" layer="95"/>
</instance>
<instance part="DIST_A" gate="-3" x="-2.54" y="71.12" smashed="yes">
<attribute name="NAME" x="0" y="70.358" size="1.524" layer="95"/>
</instance>
<instance part="DIST_A" gate="-4" x="-2.54" y="68.58" smashed="yes">
<attribute name="NAME" x="0" y="67.818" size="1.524" layer="95"/>
</instance>
<instance part="DIST_A" gate="-5" x="-2.54" y="66.04" smashed="yes">
<attribute name="NAME" x="0" y="65.278" size="1.524" layer="95"/>
</instance>
<instance part="DIST_A" gate="-6" x="-2.54" y="63.5" smashed="yes">
<attribute name="NAME" x="0" y="62.738" size="1.524" layer="95"/>
</instance>
<instance part="DIST_A" gate="-7" x="-2.54" y="60.96" smashed="yes">
<attribute name="NAME" x="0" y="60.198" size="1.524" layer="95"/>
</instance>
<instance part="DIST_B" gate="-1" x="50.8" y="76.2" smashed="yes">
<attribute name="NAME" x="53.34" y="75.438" size="1.524" layer="95"/>
<attribute name="VALUE" x="50.038" y="77.597" size="1.778" layer="96"/>
</instance>
<instance part="DIST_B" gate="-2" x="50.8" y="73.66" smashed="yes">
<attribute name="NAME" x="53.34" y="72.898" size="1.524" layer="95"/>
</instance>
<instance part="DIST_B" gate="-3" x="50.8" y="71.12" smashed="yes">
<attribute name="NAME" x="53.34" y="70.358" size="1.524" layer="95"/>
</instance>
<instance part="DIST_B" gate="-4" x="50.8" y="68.58" smashed="yes">
<attribute name="NAME" x="53.34" y="67.818" size="1.524" layer="95"/>
</instance>
<instance part="DIST_B" gate="-5" x="50.8" y="66.04" smashed="yes">
<attribute name="NAME" x="53.34" y="65.278" size="1.524" layer="95"/>
</instance>
<instance part="DIST_B" gate="-6" x="50.8" y="63.5" smashed="yes">
<attribute name="NAME" x="53.34" y="62.738" size="1.524" layer="95"/>
</instance>
<instance part="DIST_B" gate="-7" x="50.8" y="60.96" smashed="yes">
<attribute name="NAME" x="53.34" y="60.198" size="1.524" layer="95"/>
</instance>
<instance part="DIST_C" gate="-1" x="104.14" y="76.2" smashed="yes">
<attribute name="NAME" x="106.68" y="75.438" size="1.524" layer="95"/>
<attribute name="VALUE" x="103.378" y="77.597" size="1.778" layer="96"/>
</instance>
<instance part="DIST_C" gate="-2" x="104.14" y="73.66" smashed="yes">
<attribute name="NAME" x="106.68" y="72.898" size="1.524" layer="95"/>
</instance>
<instance part="DIST_C" gate="-3" x="104.14" y="71.12" smashed="yes">
<attribute name="NAME" x="106.68" y="70.358" size="1.524" layer="95"/>
</instance>
<instance part="DIST_C" gate="-4" x="104.14" y="68.58" smashed="yes">
<attribute name="NAME" x="106.68" y="67.818" size="1.524" layer="95"/>
</instance>
<instance part="DIST_C" gate="-5" x="104.14" y="66.04" smashed="yes">
<attribute name="NAME" x="106.68" y="65.278" size="1.524" layer="95"/>
</instance>
<instance part="DIST_C" gate="-6" x="104.14" y="63.5" smashed="yes">
<attribute name="NAME" x="106.68" y="62.738" size="1.524" layer="95"/>
</instance>
<instance part="DIST_C" gate="-7" x="104.14" y="60.96" smashed="yes">
<attribute name="NAME" x="106.68" y="60.198" size="1.524" layer="95"/>
</instance>
<instance part="HALL_A" gate="-1" x="-2.54" y="27.94" smashed="yes">
<attribute name="NAME" x="0" y="27.178" size="1.524" layer="95"/>
<attribute name="VALUE" x="-3.302" y="29.337" size="1.778" layer="96"/>
</instance>
<instance part="HALL_A" gate="-2" x="-2.54" y="25.4" smashed="yes">
<attribute name="NAME" x="0" y="24.638" size="1.524" layer="95"/>
</instance>
<instance part="HALL_A" gate="-3" x="-2.54" y="22.86" smashed="yes">
<attribute name="NAME" x="0" y="22.098" size="1.524" layer="95"/>
</instance>
<instance part="HALL_A" gate="-4" x="-2.54" y="20.32" smashed="yes">
<attribute name="NAME" x="0" y="19.558" size="1.524" layer="95"/>
</instance>
<instance part="HALL_A" gate="-5" x="-2.54" y="17.78" smashed="yes">
<attribute name="NAME" x="0" y="17.018" size="1.524" layer="95"/>
</instance>
<instance part="HALL_A" gate="-6" x="-2.54" y="15.24" smashed="yes">
<attribute name="NAME" x="0" y="14.478" size="1.524" layer="95"/>
</instance>
<instance part="HALL_B" gate="-1" x="50.8" y="27.94" smashed="yes">
<attribute name="NAME" x="53.34" y="27.178" size="1.524" layer="95"/>
<attribute name="VALUE" x="50.038" y="29.337" size="1.778" layer="96"/>
</instance>
<instance part="HALL_B" gate="-2" x="50.8" y="25.4" smashed="yes">
<attribute name="NAME" x="53.34" y="24.638" size="1.524" layer="95"/>
</instance>
<instance part="HALL_B" gate="-3" x="50.8" y="22.86" smashed="yes">
<attribute name="NAME" x="53.34" y="22.098" size="1.524" layer="95"/>
</instance>
<instance part="HALL_B" gate="-4" x="50.8" y="20.32" smashed="yes">
<attribute name="NAME" x="53.34" y="19.558" size="1.524" layer="95"/>
</instance>
<instance part="HALL_B" gate="-5" x="50.8" y="17.78" smashed="yes">
<attribute name="NAME" x="53.34" y="17.018" size="1.524" layer="95"/>
</instance>
<instance part="HALL_B" gate="-6" x="50.8" y="15.24" smashed="yes">
<attribute name="NAME" x="53.34" y="14.478" size="1.524" layer="95"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$1" class="0">
<segment>
<pinref part="DIST_A" gate="-1" pin="S"/>
<wire x1="-5.08" y1="76.2" x2="-15.24" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="DIST_A" gate="-2" pin="S"/>
<wire x1="-5.08" y1="73.66" x2="-15.24" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="DIST_A" gate="-3" pin="S"/>
<wire x1="-5.08" y1="71.12" x2="-15.24" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="DIST_A" gate="-4" pin="S"/>
<wire x1="-5.08" y1="68.58" x2="-15.24" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="DIST_A" gate="-5" pin="S"/>
<wire x1="-5.08" y1="66.04" x2="-15.24" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="DIST_A" gate="-6" pin="S"/>
<wire x1="-5.08" y1="63.5" x2="-15.24" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="DIST_A" gate="-7" pin="S"/>
<wire x1="-5.08" y1="60.96" x2="-15.24" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="DIST_B" gate="-1" pin="S"/>
<wire x1="48.26" y1="76.2" x2="38.1" y2="76.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="DIST_B" gate="-2" pin="S"/>
<wire x1="48.26" y1="73.66" x2="38.1" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="DIST_B" gate="-3" pin="S"/>
<wire x1="48.26" y1="71.12" x2="38.1" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="DIST_B" gate="-4" pin="S"/>
<wire x1="48.26" y1="68.58" x2="38.1" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="DIST_B" gate="-5" pin="S"/>
<wire x1="48.26" y1="66.04" x2="38.1" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="DIST_B" gate="-6" pin="S"/>
<wire x1="48.26" y1="63.5" x2="38.1" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="DIST_B" gate="-7" pin="S"/>
<wire x1="48.26" y1="60.96" x2="38.1" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="101.6" y1="76.2" x2="91.44" y2="76.2" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-1" pin="S"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="101.6" y1="73.66" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-2" pin="S"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="101.6" y1="71.12" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="101.6" y1="68.58" x2="91.44" y2="68.58" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-4" pin="S"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="101.6" y1="66.04" x2="91.44" y2="66.04" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-5" pin="S"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="101.6" y1="63.5" x2="91.44" y2="63.5" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-6" pin="S"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="101.6" y1="60.96" x2="91.44" y2="60.96" width="0.1524" layer="91"/>
<pinref part="DIST_C" gate="-7" pin="S"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="-5.08" y1="27.94" x2="-15.24" y2="27.94" width="0.1524" layer="91"/>
<pinref part="HALL_A" gate="-1" pin="S"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="-5.08" y1="25.4" x2="-15.24" y2="25.4" width="0.1524" layer="91"/>
<pinref part="HALL_A" gate="-2" pin="S"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="-5.08" y1="22.86" x2="-15.24" y2="22.86" width="0.1524" layer="91"/>
<pinref part="HALL_A" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="-5.08" y1="20.32" x2="-15.24" y2="20.32" width="0.1524" layer="91"/>
<pinref part="HALL_A" gate="-4" pin="S"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="-5.08" y1="17.78" x2="-15.24" y2="17.78" width="0.1524" layer="91"/>
<pinref part="HALL_A" gate="-5" pin="S"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="-5.08" y1="15.24" x2="-15.24" y2="15.24" width="0.1524" layer="91"/>
<pinref part="HALL_A" gate="-6" pin="S"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="48.26" y1="27.94" x2="38.1" y2="27.94" width="0.1524" layer="91"/>
<pinref part="HALL_B" gate="-1" pin="S"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="48.26" y1="25.4" x2="38.1" y2="25.4" width="0.1524" layer="91"/>
<pinref part="HALL_B" gate="-2" pin="S"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="48.26" y1="22.86" x2="38.1" y2="22.86" width="0.1524" layer="91"/>
<pinref part="HALL_B" gate="-3" pin="S"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="48.26" y1="20.32" x2="38.1" y2="20.32" width="0.1524" layer="91"/>
<pinref part="HALL_B" gate="-4" pin="S"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="48.26" y1="17.78" x2="38.1" y2="17.78" width="0.1524" layer="91"/>
<pinref part="HALL_B" gate="-5" pin="S"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="48.26" y1="15.24" x2="38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="HALL_B" gate="-6" pin="S"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
